# LHC Contribution Guide

### Prefacing:
  * 2FA is a requirement.
  * Use SSH for pushing and pulling: [Here's how to do it for GitLab](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
  * GPG sign all of your commits [Here's how to set that up](https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md)
### Making changes:
  * Before the before:
  	* [Fork the repo](https://docs.gitlab.com/ee/workflow/forking_workflow.html)
  		* Already have a fork?
  			* (Easy) Delete your fork and refork
  			* [Efficient](https://stefanbauer.me/articles/how-to-keep-your-git-fork-up-to-date)
  			* [Automated](https://docs.gitlab.com/ee/workflow/repository_mirroring.html)
  	* [In depth GitLab workflow that's way over what you need to know, but is good for understanding git](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
  * Before making any changes:
  	* Pull down from remote to master. `git pull <remote> master` (`<remote>` will likely be `origin`)
  	* Checkout a new branch `git checkout -b <branch_name>`
  		* Name the branch in a way thats semi-related to what you're doing.
  * Makings Changes
  	* Try to make a commit only do one thing and only thing only if possible.  This makes doing and handling regressions much easier.
  	* Try to keep lines below 80/140 characters.  If there's a good reason it needs to be longer that's fine.  Just the longer a line gets, the more difficult it is to quickly navigate.
  * Adding changes for committing
  	* Basic git usage: `git add <file>`
  	* Removing a file from being tracked in git `git rm <file>`
  	* Granular commit adding `git add -p <file>`
  * Making a commit
  	* Basic command `git commit -S` (this will then launch your $EDITOR, default text editor).
  		* [How I change/set my default editor for git?](https://www.git-scm.com/book/en/v2/Customizing-Git-Git-Configuration)
  	* [Why and how to write decent commit messages](https://chris.beams.io/posts/git-commit/)
  * Pushing to a remote
  	* Basic command `git push <remote> <branch_name>`
  	* Am I done? Close
  * Merging your commit to master (AKA the code actually used)
  	* [Making a Pull Request (PR)](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
  	* Why do we bother with this?
  		* It allows other people to review what you're doing
  		* Helps keep everyone in the new
  		* Often helps catch mistakes before they start being used in product.
  		* [A bit more about PR's](https://www.atlassian.com/git/tutorials/making-a-pull-request)


### Wait what, I'm new to git and I'm confused as hell.
![(https://xkcd.com/1597/)If that doesn't fix it, git.txt contains the phone number of a friend of mine who understands git. Just wait through a few minutes of 'It's really pretty simple, just think of branches as...' and eventually you'll learn the commands that will fix everything.](https://imgs.xkcd.com/comics/git.png)

The basics of git are pretty simple here's a few things you can read over to kind of figure things out.
* [Understanding what git is and the concepts associated with it](https://dev.to/unseenwizzard/learn-git-concepts-not-commands-4gjc)
* [I've messed up so bad, I have no idea what I've done and need a step by step walk through](http://sethrobertson.github.io/GitFixUm/fixup.html)
* [Squashing Commits and Rebasing](https://blog.carbonfive.com/2017/08/28/always-squash-and-rebase-your-git-commits/) (AKA keeping commits cleans)
* [Git reflog](https://www.atlassian.com/git/tutorials/rewriting-history/git-reflog)
* [The reference manual](https://www.git-scm.com/docs)
* The magic alias for reseting your local branch to the same state as the remote: ```alias gpristine='git reset --hard && git clean -dfx'```

## I'm lost and need help
```@Kiro47#6101 || <@217674453075623952> Plz Help, Lost in git```

